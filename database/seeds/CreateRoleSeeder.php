<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class CreateRoleSeeder extends Seeder
{
  public function run()
  {
    app()[PermissionRegistrar::class]->forgetCachedPermissions();

    $pengurus = Role::create(['name' => 'pengurus']);
    $petugas = Role::create(['name' => 'petugas']);

    $user = User::create([
      'name' => 'Akun Pengurus',
      'username' => 'pengurus',
      'password' => Hash::make('pengurus'),
    ]);
    $user->assignRole($pengurus);

    $user = User::create([
      'name' => 'Akun Petugas',
      'username' => 'petugas',
      'password' => Hash::make('petugas'),
    ]);
    $user->assignRole($petugas);
  }
}
