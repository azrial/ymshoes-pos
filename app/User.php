<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
  use Notifiable, HasRoles;

  protected $table = "users";

  protected $fillable = [
    'name', 'username', 'password',
  ];

  protected $hidden = [
    'password',
  ];
}
