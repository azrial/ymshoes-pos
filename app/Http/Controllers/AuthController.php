<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
  public function __construct()
  {
    $this->middleware('guest');
  }
  public function login(Request $request)
  {
    return view('login')->with([
      'notif' => json_decode(json_encode($request->session()->get('notif'))),
    ]);
  }
}
