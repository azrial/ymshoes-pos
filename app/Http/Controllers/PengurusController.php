<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PengurusController extends Controller
{

  public function sidebar($menuActive)
  {
    switch ($menuActive) {
      case 'Dashboard':
        $dashboard = true;
        break;
      case 'Sepatu':
        $sepatu = true;
        break;
    }

    $menu = [
      [
        'name' => 'Dashboard',
        'link' => route('pengurus.dashboard'),
        'isActive' => $dashboard ?? false,
      ],
      [
        'name' => 'Sepatu',
        'link' => route('pengurus.sepatu'),
        'isActive' => $sepatu ?? false,
      ],
    ];
    return $menu;
  }

  public function dashboard(Request $request)
  {
    $sidebar = $this->sidebar('Dashboard');

    return view('pengurus.dashboard')->with([
      'sidebar' => json_decode(json_encode($sidebar)),
    ]);
  }

  public function sepatu(Request $request)
  {
    $sidebar = $this->sidebar('Sepatu');

    return view('pengurus.sepatu/sepatu')->with([
      'sidebar' => json_decode(json_encode($sidebar)),
    ]);
  }
}
