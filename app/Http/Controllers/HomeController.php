<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */
  public function index()
  {
    if (auth()->user()->hasAnyRole('pengurus')) {
      return redirect()->route('pengurus');
    } else if (auth()->user()->hasAnyRole('petugas')) {
      return redirect()->route('petugas');
    } else {
      return redirect()->route('masuk')->with([
        'notif' => [
          'status' => 'error',
          'pesan' => 'akses dilarang',
        ]
      ]);
    }
  }
}
