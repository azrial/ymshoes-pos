<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>{{ $title . ' | YMSHOES' }}</title>

  {{-- js --}}
  <script src="{{ asset('js/app.js') }}" defer></script>
  {{-- css --}}
  <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  <link rel="stylesheet" href="{{ asset('fontawesome/css/all.min.css') }}">
</head>

<body>
  <div id="app">
    @yield('content')
  </div>

  {{-- js --}}
  <script src="{{ asset('js/main.js') }}" defer></script>
</body>

</html>
