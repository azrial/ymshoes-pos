@extends('layouts.master', [
'title' => $title . ' | Pengurus',
'sidebar' => $sidebar,
])

@section('content')
<section class="flex w-screen h-screen">

  {{-- sidebar --}}
  <div class="w-64 h-screen overflow-y-auto bg-red-primary">

    {{-- header --}}
    <div class="flex items-center justify-center w-full h-16 border-b border-black-10">
      <h1 class="text-2xl font-bold text-white">YMSHOES</h1>
    </div>

    {{-- menu --}}
    <div class="container py-4 -my-2">
      @foreach ($sidebar as $menu)
      <a href="{{ $menu->link }}" class="sidebar_menu @if($menu->isActive) active @else hover:text-white @endif">
        {{ $menu->name }}
      </a>
      @endforeach
    </div>

  </div>
  {{-- sidebar end --}}

  {{-- content --}}
  <div class="flex-1 overflow-y-auto bg-gray-100">

    {{-- navbar --}}
    <nav class="w-full h-16 bg-white border-b border-black-10">
      <div class="container flex items-center justify-between h-full">

        <h1 class="text-lg font-light text-gray-900">
          Hai, <b class="font-semibold">{{ auth()->user()->name }}</b>
        </h1>

        <a href="{{ route('logout') }}" class="px-4 py-1 text-red-600 duration-200 border border-red-600 rounded cursor-pointer hover:bg-red-600 hover:text-gray-100" onclick="event.preventDefault(); document.querySelector('#form_logout').submit();">Keluar</a>
        <form id="form_logout" class="hidden" action="{{ route('logout') }}" method="post">
          @csrf
        </form>

      </div>
    </nav>

    {{-- isi --}}
    <div class="min-h-screen py-4">
      <div class="container -my-4">
        @yield('contentp')
      </div>
    </div>

    {{-- footer --}}
    <footer class="h-10 bg-white border-t border-black-10">
      <div class="container flex items-center justify-between h-full">
        <span class="font-light text-gray-800">
          Built By <a href="https://facebook.com/azrialmons" target="_blank" class="font-medium duration-200 hover:text-red-primary">Muhammad Azrial</a>
        </span>
        <span class="font-light text-gray-800">
          Copyright {{ date('Y') }}. <a href="https://instagram.com/ymshoes28" target="_blank" class="font-medium duration-200 hover:text-red-primary">YMSHOES</a>
        </span>
      </div>
    </footer>
    {{-- footer end --}}

  </div>
  {{-- content end --}}


</section>
@endsection
