@extends('pengurus.layouts/pengurus', [
  'title' => 'Dashboard',
  'sidebar' => $sidebar,
])

@section('contentp')

{{-- card jumlah --}}
<div class="my-4">
  <div class="grid grid-cols-12 gap-4">

    {{-- jumlah sepatu --}}
    <cardjumlah color="#fa744f" title="Jumlah Sepatu" jumlah="0"></cardjumlah>

    {{-- jumlah merk --}}
    <cardjumlah color="#4592af" title="Jumlah Merk" jumlah="0"></cardjumlah>

    {{-- jumlah jenis --}}
    <cardjumlah color="#639a67" title="Jumlah Jenis" jumlah="0"></cardjumlah>

  </div>
</div>
{{-- card jumlah end --}}

{{-- grafik penjualan mingguan --}}
<div class="my-4">
  <div class="grid grid-cols-12 gap-5">
    <div class="col-span-12 px-4 py-2 bg-white rounded shadow-md">

      {{-- header --}}
      <div class="flex items-center justify-between">
        <h1 class="title">Grafik Penjualan Mingguan</h1>
        <span class="text-sm text-black-45">Senin, 20 April 2020 &mdash; Minggu, 26 April 2020</span>
      </div>

      {{-- grafik --}}
      <div class="mt-2">
        <gpenjualan :datapmingguan="[1, 4, 1, 6, 3, 6, 2]"></gpenjualan>
      </div>

    </div>
  </div>
</div>
{{-- grafik penjualan mingguan end --}}

{{-- grafik perang merk dan jenis --}}
<div class="my-4">
  <div class="grid grid-cols-12 gap-4">

    {{-- merk --}}
    <div class="col-span-7 px-4 py-2 bg-white rounded shadow-md">

      {{-- header --}}
      <div class="flex items-center justify-between">
        <h1 class="title">Grafik Merk Terlaris</h1>
      </div>

      {{-- grafik --}}
      <div class="mt-2">
        <gmerklaris :merk="['Compass', 'Advan', 'Yezze']" :pmerk="[4, 9, 3]"></gmerklaris>
      </div>

    </div>

    {{-- jenis --}}
    <div class="col-span-5 px-4 py-2 bg-white rounded shadow-md">

      {{-- header --}}
      <div class="flex items-center justify-between">
        <h1 class="title">Grafik Jenis Terlaris</h1>
        <input type="text" class="px-4 py-1 text-xs placeholder-gray-400 appearance-none input-primary focus:border-red-primary focus:text-red-primary" placeholder="Masukkan Merk..." list="merks">
        <datalist id="merks">
          <option value="Internet Explorer">
          <option value="Firefox">
          <option value="Chrome">
          <option value="Opera">
          <option value="Safari">
        </datalist>
      </div>

      {{-- grafik --}}
      <div class="mt-2">
        <gjenislaris :jenis="['Jenis 1', 'Jenis 2', 'Jenis 3', 'Jenis 4']" :pjenis="[164, 321, 80, 492]"></gjenislaris>
      </div>

    </div>

  </div>
</div>
{{-- grafik perang merk dan jenis end --}}

@endsection
