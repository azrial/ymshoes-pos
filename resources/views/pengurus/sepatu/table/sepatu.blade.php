{{-- header --}}
<div class="flex items-center justify-between">
  <h1 class="title">Tabel Daftar Sepatu</h1>
  <input type="text" placeholder="Cari Sepatu..." class="w-64 px-4 py-1 rounded input-primary focus:border-red-primary focus:text-red-primary" autocomplete="off">
</div>

{{-- isi --}}
<div class="mt-2">

  {{-- table --}}
  <div class="flex flex-col w-full overflow-hidden text-center rounded">

    {{-- header table --}}
    <div class="flex border border-gray-800">
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 5%;">#</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 20%;">Merk</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 20%;">Jenis</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 15%;">Warna</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 15%;">Ukuran</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 15%;">Stok</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 10%;"></span>
    </div>

    {{-- content table --}}
    <div class="flex flex-col overflow-hidden border rounded-b">

      {{-- data --}}
      <div class="flex items-center even:bg-gray-100">
        <span class="py-2" style="width: 5%;">1</span>
        <span class="py-2" style="width: 20%;">Asus</span>
        <span class="py-2" style="width: 20%;">X441UV</span>
        <span class="py-2" style="width: 15%;">Hitam</span>
        <span class="py-2" style="width: 15%;">14</span>
        <span class="py-2" style="width: 15%;">142</span>
        <span class="py-2" style="width: 10%;">
          <a href="" class="inline-flex items-center justify-center w-8 h-8 overflow-hidden text-blue-800 duration-200 bg-blue-200 rounded hover:text-blue-900 hover:bg-blue-300"><i class="fas fa-eye fa-fw"></i></a>
        </span>
      </div>

    </div>
  </div>
  {{-- table end --}}

</div>
