{{-- header --}}
<div class="flex items-center justify-between">
  <h1 class="title">Tabel Daftar Merk</h1>
  <input type="text" placeholder="Cari Merk..." class="w-64 px-4 py-1 rounded input-primary focus:border-red-primary focus:text-red-primary" autocomplete="off">
</div>

{{-- isi --}}
<div class="mt-2">

  {{-- table --}}
  <div class="flex flex-col w-full overflow-hidden text-center rounded">

    {{-- header table --}}
    <div class="flex border border-gray-800">
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 5%;">#</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 45%;">Merk</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 20%;">Jumlah Jenis</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 20%;">Jumlah Stok</span>
      <span class="py-2 font-medium text-white bg-gray-800" style="width: 10%;"></span>
    </div>

    {{-- content table --}}
    <div class="flex flex-col overflow-hidden border rounded-b">

      {{-- data --}}
      <div class="flex items-center even:bg-gray-100">
        <span class="py-2" style="width: 5%;">1</span>
        <span class="py-2" style="width: 45%;">Asus</span>
        <span class="py-2" style="width: 20%;">6</span>
        <span class="py-2" style="width: 20%;">142</span>
        <span class="py-2" style="width: 10%;">
          <a href="" class="inline-flex items-center justify-center w-8 h-8 overflow-hidden text-blue-800 duration-200 bg-blue-200 rounded hover:text-blue-900 hover:bg-blue-300"><i class="fas fa-eye fa-fw"></i></a>
        </span>
      </div>

    </div>
  </div>
  {{-- table end --}}

</div>
