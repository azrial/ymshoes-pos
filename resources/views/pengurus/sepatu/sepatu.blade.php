@extends('pengurus.layouts/pengurus', [
'title' => 'Sepatu',
'sidebar' => $sidebar,
])

@section('contentp')

{{-- header cta --}}
<div class="my-4">
  <div class="flex items-center justify-between">

    {{-- cta sepatu/merk --}}
    <div class="-mx-1">
      <a id="cta_sm" data-cta="sepatu" class="btn-cta hover:bg-red-primary hover:text-white @if(Request::is('pengurus/sepatu')) active @endif">Sepatu</a>
      <a id="cta_sm" data-cta="merk" class="btn-cta hover:bg-red-primary hover:text-white @if(Request::is('pengurus/merk')) active @endif">Merk</a>
    </div>

    {{-- tambah sepatu/merk --}}
    <div class="-mx-1">
      <a href="" id="cta_ts" class="inline-block px-4 py-2 mx-1 text-center text-gray-800 duration-200 bg-green-200 rounded hover:bg-green-300 hover:text-gray-900">Tambah Sepatu</a>
      <a href="" id="cta_tm" class="hidden inline-block px-4 py-2 mx-1 text-center text-gray-800 duration-200 bg-green-200 rounded hover:bg-green-300 hover:text-gray-900">Tambah Merk</a>
    </div>

  </div>
</div>
{{-- header cta end --}}

{{-- table --}}
<div class="my-4">

  {{-- sepatu --}}
  <div id="table_s" class="hidden w-full px-4 py-2 bg-white rounded shadow-md">
    @include('pengurus.sepatu/table/sepatu')
  </div>
  {{-- sepatu end --}}

  {{-- merk --}}
  <div id="table_m" class="hidden w-full px-4 py-2 bg-white rounded shadow-md">
    @include('pengurus.sepatu/table/merk')
  </div>
  {{-- merk end --}}

</div>
{{-- table end --}}

@endsection
