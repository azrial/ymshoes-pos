@extends('layouts.master', [
  'title' => 'Login'
])

@section('content')
<div class="flex items-center justify-center w-screen h-screen bg-gray-200">

  {{-- box white --}}
  <div class="w-4/12 overflow-hidden bg-white rounded shadow">
    {{-- header --}}
    <div class="flex items-center w-full h-16 px-4 bg-red-primary">
      <h1 class="text-2xl font-bold tracking-wide text-white uppercase text-shadow">Login Dong</h1>
    </div>

    {{-- form --}}
    <form class="px-4 py-4 mt-6 -my-1" method="POST" action="{{ route('login') }}">
      @csrf

      {{-- notifikasi --}}
      @if ($notif)
        <notifikasi status="{{ $notif->status }}" pesan="{{ $notif->pesan }}"></notifikasi>
      @endif
      {{-- notifikasi end --}}

      <div class="my-1">
        <input class="w-full px-4 py-2 placeholder-gray-400 input-primary focus:border-red-primary focus:text-red-primary" type="text" placeholder="Masukkan Username Anda..." autocomplete="off" name="username" autofocus value="{{ old('username') }}">
      </div>
      <div class="my-1">
        <input class="w-full px-4 py-2 placeholder-gray-400 input-primary focus:border-red-primary focus:text-red-primary" type="password" placeholder="Masukkan Password Anda..." autocomplete="off" name="password">
      </div>
      <div class="my-1 mt-2">
        <button type="submit" class="w-full px-4 py-2 font-medium text-gray-100 duration-200 rounded bg-red-primary hover:bg-red-primary-dark hover:text-white focus:outline-none">Masuk</button>
      </div>
    </form>
  </div>
  {{-- box white end --}}

</div>
@endsection
