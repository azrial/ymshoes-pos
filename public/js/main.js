let ctas = document.querySelectorAll("#cta_sm");
let ctats = document.querySelector("#cta_ts");
let ctatm = document.querySelector("#cta_tm");
let tables = document.querySelector("#table_s");
let tablem = document.querySelector("#table_m");

let text = document.querySelector("#cta_sm.active").getAttribute("data-cta");
if (text == "sepatu") {
  ctats.classList.remove("hidden");
  tables.classList.remove("hidden");
  ctatm.classList.add("hidden");
  tablem.classList.add("hidden");
  window.history.pushState("", "", "/pengurus/sepatu");
  document.title = "Sepatu | Pengurus | YMSHOES";
} else if (text == "merk") {
  ctats.classList.add("hidden");
  tables.classList.add("hidden");
  ctatm.classList.remove("hidden");
  tablem.classList.remove("hidden");
  window.history.pushState("", "", "/pengurus/merk");
  document.title = "Merk | Pengurus | YMSHOES";
}

for (let i = 0; i < ctas.length; i++) {
  ctas[i].addEventListener("click", function() {
    let current = document.querySelectorAll("#cta_sm.active");
    console.log(current);
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";

    text = this.getAttribute("data-cta");
    if (text == "sepatu") {
      ctats.classList.remove("hidden");
      tables.classList.remove("hidden");
      ctatm.classList.add("hidden");
      tablem.classList.add("hidden");
      window.history.pushState("", "", "/pengurus/sepatu");
      document.title = "Sepatu | Pengurus | YMSHOES";
    } else if (text == "merk") {
      ctats.classList.add("hidden");
      tables.classList.add("hidden");
      ctatm.classList.remove("hidden");
      tablem.classList.remove("hidden");
      window.history.pushState("", "", "/pengurus/merk");
      document.title = "Merk | Pengurus | YMSHOES";
    }
  });
}
