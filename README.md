## Aplikasi Kasir

- Laravel 7 sebagai backend
- Tailwindcss sebagai frontend

**Yang Sudah Dibuat**

- [x] Setup Tailwindcss
- [x] Database
  - [x] User
  - [x] Role
- [x] Halaman Login _(frontend)_ _(backend)_
- [x] Hak Akses _(backend)_
  - [x] Pengurus
    - [x] Dashboard
      - [x] Card Jumlah _(frontend)_
      - [x] Grafik Penjualan Mingguan _(frontend)_
      - [x] Grafik Merk dan Jenis Terlaris _(frontend)_
    - [x] Sepatu
      - [x] Tombol Switch Sepatu/Merk
      - [x] Tabel
        - [x] Sepatu _(frontend)_
        - [x] Merk _(frontend)_
