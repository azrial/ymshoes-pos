<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return redirect()->route('masuk');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

// VIEW's
Route::get('masuk', 'AuthController@login')->name('masuk');

// Pengurus
Route::group(['middleware' => ['role:pengurus']], function () {
  Route::get('pengurus', function () {
    return redirect()->route('pengurus.dashboard');
  })->name('pengurus');
  Route::get('pengurus/dashboard', 'PengurusController@dashboard')->name('pengurus.dashboard');
  Route::get('pengurus/sepatu', 'PengurusController@sepatu')->name('pengurus.sepatu');
  Route::get('pengurus/merk', 'PengurusController@sepatu')->name('pengurus.merk');
});
