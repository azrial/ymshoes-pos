module.exports = {
  theme: {
    extend: {
      colors: {
        "red-primary": "#d7385e",
        "red-primary-dark": "#bf2146",

        "black-5": "rgba(0, 0, 0, 5%)",
        "black-10": "rgba(0, 0, 0, 10%)",
        "black-15": "rgba(0, 0, 0, 15%)",
        "black-20": "rgba(0, 0, 0, 20%)",
        "black-25": "rgba(0, 0, 0, 25%)",
        "black-30": "rgba(0, 0, 0, 30%)",
        "black-35": "rgba(0, 0, 0, 35%)",
        "black-40": "rgba(0, 0, 0, 40%)",
        "black-45": "rgba(0, 0, 0, 45%)",
        "black-50": "rgba(0, 0, 0, 50%)",
        "black-55": "rgba(0, 0, 0, 55%)",
        "black-60": "rgba(0, 0, 0, 60%)",
        "black-65": "rgba(0, 0, 0, 65%)",
        "black-70": "rgba(0, 0, 0, 70%)",
        "black-75": "rgba(0, 0, 0, 75%)",
        "black-80": "rgba(0, 0, 0, 80%)",

        "white-5": "rgba(255, 255, 255, 5%)",
        "white-10": "rgba(255, 255, 255, 10%)",
        "white-15": "rgba(255, 255, 255, 15%)",
        "white-20": "rgba(255, 255, 255, 20%)",
        "white-25": "rgba(255, 255, 255, 25%)",
        "white-30": "rgba(255, 255, 255, 30%)",
        "white-35": "rgba(255, 255, 255, 35%)",
        "white-40": "rgba(255, 255, 255, 40%)",
        "white-45": "rgba(255, 255, 255, 45%)",
        "white-50": "rgba(255, 255, 255, 50%)",
        "white-55": "rgba(255, 255, 255, 55%)",
        "white-60": "rgba(255, 255, 255, 60%)",
        "white-65": "rgba(255, 255, 255, 65%)",
        "white-70": "rgba(255, 255, 255, 70%)",
        "white-75": "rgba(255, 255, 255, 75%)",
        "white-80": "rgba(255, 255, 255, 80%)"
      }
    },
    container: {
      center: true,
      padding: "1rem"
    }
  },
  variants: {
    backgroundColor: [
      "responsive",
      "hover",
      "focus",
      "group-hover",
      "even",
      "odd"
    ]
  },
  plugins: []
};
